package com.dianakisil.testdocobo;

public class EcgData extends MeasuredData {
    private double recordTime = 0.000;
    private int dataId;
    private static double RECORDRATE = 1.0/500.0;


    public EcgData(int data, int dataId) {
        super("ECG", data);
        this.dataId = dataId;
        recordTime = (double) dataId * RECORDRATE;
    }

    public double getRecordTime() {
        return recordTime;
    }

    public int getDataId() {
        return dataId;
    }

    public static void setRecordRate (double herz){
        RECORDRATE = 1.0/herz;
    }

    public static double getRECORDRATE (){
        return RECORDRATE;
    }
}
