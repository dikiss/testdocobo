package com.dianakisil.testdocobo;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private final int PORT = 0;
    private final int LAND = 1;
    private int PORTORLAND = 0;
    private final int BLUETOOTH_PERMISSION_REQ = 11;
    private final int BLUETOOTHADMIN_PERMISSION_REQ = 12;
    private final int WRITEFILE_PERMISSION_REQ = 13;
    private final int COARSE_PERMISSION_REQ = 14;


    private EcgDatabaseManager ecgDatabaseManager;
    private TextView beatRateText;
    private TextView rpeakValue;
    private TextView spo2Value;
    private RadioGroup displaySelectionGroup;
    private boolean receiveSpo2 = false;
    private boolean hasConnected = false;
    private EditText rateSettinginEdit;
    private String rateSettinginString = new String();

    private boolean bleIsGranted = false;
    private boolean bleAdminIsGranted = false;
    private boolean fileWriteIsGranted = false;
    private boolean coarseLocIsGranted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void portLoading() {
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.portToolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    private void landLoading() {
        setContentView(R.layout.activity_main_land);


        beatRateText = (TextView) findViewById(R.id.land_beat_rate_text);
        rpeakValue = (TextView) findViewById(R.id.land_rr_text);
        PORTORLAND = LAND;
    }

    @Override
    protected void onResume() {
        super.onResume();
        hasConnected = true;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {

            case R.id.spo2_switch:
                if (receiveSpo2) {
                    item.setTitle("Start");
                    receiveSpo2 = false;
                }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}